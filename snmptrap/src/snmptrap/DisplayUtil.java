package snmptrap;

import snmptrap.traps.Trap;

public class DisplayUtil
{
	public static void show(Trap trap)
	{
		System.out.println("Trap Info (OID:IP:Port:Timeticks) => " +
							trap.getOid() 		   + ":" +
							trap.getAddressParam() + ":" + 
							trap.getPortParam()    + ":" +
							trap.getTimeticks());
	}
}
