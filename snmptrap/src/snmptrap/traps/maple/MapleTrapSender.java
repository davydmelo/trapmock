package snmptrap.traps.maple;

import org.json.simple.JSONObject;

public class MapleTrapSender {
	
	public static void caseC11_1Success1(JSONObject params, String addressParam, int portParam) {

		long millis = System.currentTimeMillis()/1000;

		try
		{
			new TrapOlt1("1-1", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC11_2Success1(JSONObject params, String addressParam, int portParam) {

		long millis = System.currentTimeMillis()/1000;

		try
		{
			new TrapOlt2("1-2", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família MAPLE - Caso C14.1                         */
	/* ----------------------------------------------- */
	public static void caseC14_1Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap1("1-1", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C14.2                         */
	/* ----------------------------------------------- */
	public static void caseC14_2Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap2("1-2", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família MAPLE - Caso C7                         */
	/* ----------------------------------------------- */
	
	public static void caseC7Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new Trap25("1-25", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new Trap16("2-16", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família MAPLE - Caso C2                         */
	/* ----------------------------------------------- */
	
	public static void caseC2Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new Trap16("1-16", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família MAPLE - Caso C5                         */
	/* ----------------------------------------------- */
	
	public static void caseC5Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new Trap5("1-5", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new Trap16("2-16", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC5Fail1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			//new Trap5("1-5", params, addressParam, portParam, millis + 0).run();
			//Thread.sleep(50);
			
			new Trap16("2-16", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC5Fail2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new Trap5("1-5", params, addressParam, portParam, millis + 0).run();
			//Thread.sleep(50);
			
			//new Trap16("2-16", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família MAPLE - Caso C8                         */
	/* ----------------------------------------------- */
	
	public static void caseC8Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new Trap6("1-6", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new Trap23("2-23", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);
			
			new Trap14("3-14", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);
			
			new Trap12("4-12", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);
			
			new Trap13("5-13", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
