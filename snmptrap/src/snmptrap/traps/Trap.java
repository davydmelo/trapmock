package snmptrap.traps;

import org.json.simple.JSONObject;

public abstract class Trap {

	protected JSONObject params;
	protected String addressParam;
	protected int portParam;
	protected Long timeticks;
	protected String oid;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public JSONObject getParams() {
		return params;
	}

	public void setParams(JSONObject params) {
		this.params = params;
	}

	public String getAddressParam() {
		return addressParam;
	}

	public void setAddressParam(String addressParam) {
		this.addressParam = addressParam;
	}

	public int getPortParam() {
		return portParam;
	}

	public void setPortParam(int portParam) {
		this.portParam = portParam;
	}

	public Long getTimeticks() {
		return timeticks;
	}

	public void setTimeticks(Long timeticks) {
		this.timeticks = timeticks;
	}
}
