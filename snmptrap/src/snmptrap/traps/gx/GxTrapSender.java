package snmptrap.traps.gx;

import org.json.simple.JSONObject;

public class GxTrapSender {
	
	public static void caseC12_1_OLT_Success(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new snmptrap.traps.gx.olt.Trap39("1-39", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC12_2_1_OLT_Success(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new snmptrap.traps.gx.olt.Trap42("1-42", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC12_2_2_OLT_Success(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new snmptrap.traps.gx.olt.Trap40("1-40", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC5_1_OLT_Success(JSONObject params, String addressParam, int portParam) 
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new snmptrap.traps.gx.olt.Trap10("1-10", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new snmptrap.traps.gx.olt.Trap6("2-6", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC5_2_OLT_Success(JSONObject params, String addressParam, int portParam) 
	{
		long millis = System.currentTimeMillis()/1000;
		
		try
		{
			new snmptrap.traps.gx.olt.Trap9("1-9", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new snmptrap.traps.gx.olt.Trap5("2-5", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC14_1Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap217Down("1-217", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}
	
	public static void caseC14_2Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap217Up("1-217", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	
	/* ----------------------------------------------- */
	/* Família GX - Caso C11                         */
	/* ----------------------------------------------- */
	public static void caseC11_1Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap219("1-219", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}
	
	public static void caseC11_2Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap220("1-220", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}


	/* ----------------------------------------------- */
	/* Família GX - Caso C2                            */
	/* ----------------------------------------------- */

	public static void caseC2Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);			

			new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Success2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Success3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			//new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			//Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Success4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			//new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
			//Thread.sleep(50);

			//new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			//Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Success5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			//new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			//Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap260("3-260", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 201).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 402).run();
			Thread.sleep(50);

			//new Trap201("4-201", params, addressParam, portParam, millis + 603).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 201).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 402).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 603).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 200).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 300).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 100).run();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 3000).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap201("4-201", params, addressParam, portParam, millis + 2000).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 5000).run();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC2Fail6(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 200).run();
			Thread.sleep(50);

			new Trap260("3-260", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			//new Trap201("4-201", params, addressParam, portParam, millis + 100).run();
			//Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + 300).run();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	/* ----------------------------------------------- */
	/* Família GX - Caso C5                            */
	/* ----------------------------------------------- */

	public static void caseC5Fail6(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap201("5-201", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 40).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap201("5-201", params, addressParam, portParam, millis + 402).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 804).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 201).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 603).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap201("5-201", params, addressParam, portParam, millis + 804).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 201).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 402).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 603).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	public static void caseC5Fail3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 201).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 402).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 603).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 804).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	public static void caseC5Fail2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			//			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}			
	}

	public static void caseC5Fail1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();

			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			//new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			//Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success4(JSONObject params, String addressParam, int portParam) {
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}

	public static void caseC5Success3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			//new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			//Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	public static void caseC5Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap260("2-260", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap244("3-244", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap223("4-223", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + 40).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	/* ----------------------------------------------- */
	/* Família GX - Caso C8                            */
	/* ----------------------------------------------- */

	public static void caseC8Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap224("1-224", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap222("2-222", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap200("3-200", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
