package snmptrap.traps.ld;

import org.json.simple.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import snmptrap.DisplayUtil;
import snmptrap.traps.Trap;

public class Trap42 extends Trap implements Runnable
{
	public Trap42(JSONObject params, String addressParam, int portParam)
	{
		this.params = params;
		this.addressParam = addressParam;
		this.portParam = portParam;
	}

	public Trap42(String oid, JSONObject params, String addressParam, int portParam, Long timeticks)
	{
		this.oid = oid;
		this.params = params;
		this.addressParam = addressParam;
		this.portParam = portParam;
		this.timeticks = timeticks;
	}

	@Override
	public void run()
	{
		try
		{
			PDU pdu = new PDU();
			pdu.setType(PDU.TRAP);
			pdu.setRequestID(new Integer32(123));

			pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new TimeTicks(this.timeticks)));
			pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID("1.3.6.1.4.1.3979.6.4.2.1.2.5.1.0.42")));
			//pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.8.9.1.1.0"), new IpAddress(params.get("olt_ip").toString())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.3979.6.4.2.1.1.5.4.1.1.1"), new Integer32(((Long)params.get("gpon_olt_index")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.3979.6.4.2.1.1.2.2.4.2.1.2"), new Integer32(((Long)params.get("onu_id")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.3979.6.4.2.1.1.2.2.4.2.1.3"), new OctetString(params.get("onu_serial_number").toString())));

			TransportMapping<?> transport = new DefaultUdpTransportMapping();

			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString("public"));
			comtarget.setVersion(SnmpConstants.version2c);
			comtarget.setAddress(new UdpAddress(addressParam + "/" + portParam));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			Snmp snmp = new Snmp(transport);
			snmp.send(pdu, comtarget);
			
			DisplayUtil.show(this);
			
			snmp.close();
		}
		catch (Exception e)
		{
			System.err.println("Error in Sending Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			System.err.println("Exception Message = " + e.getMessage());
		}

	}
}

