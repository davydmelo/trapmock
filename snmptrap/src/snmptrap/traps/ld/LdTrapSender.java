package snmptrap.traps.ld;

import org.json.simple.JSONObject;
import snmptrap.traps.ld.Trap1;
import snmptrap.traps.ld.Trap2;

public class LdTrapSender {
	
	public static void caseC5_1_OLT_Success(JSONObject params, String addressParam, int portParam) 
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new snmptrap.traps.ld.olt.Trap32("1-32", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC5_2_OLT_Success(JSONObject params, String addressParam, int portParam) 
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new snmptrap.traps.ld.olt.Trap33("1-33", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}
	
	public static void caseC11_1Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new TrapOlt1("1-1", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void caseC11_2Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new TrapOlt2("1-2", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C14.1                         */
	/* ----------------------------------------------- */
	public static void caseC14_1Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap1("1-1", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C14.2                         */
	/* ----------------------------------------------- */
	public static void caseC14_2Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap2("1-2", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C7                            */
	/* ----------------------------------------------- */
	public static void caseC7Success1(JSONObject params, String addressParam, int portParam) {
		
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap70("2-42", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new Trap21("2-42", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);
			
			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C2                            */
	/* ----------------------------------------------- */

	public static void caseC2Success4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap42("2-42", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
//			new Trap17("1-17", params, addressParam, portParam, millis + 30).run();
//			Thread.sleep(50);
			
			new Trap21("2-21", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);

			new Trap42("3-42", params, addressParam, portParam, millis + 30).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap42("2-42", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap21("1-21", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			//new Trap42("2-42", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap21("1-21", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 1201).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap42("2-42", params, addressParam, portParam, millis + 1201).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 0).run();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap42("2-42", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 1201).run();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC2Fail6(JSONObject params, String addressParam, int portParam)
	{
		/* N/A -> Se remover a trap opcional caímos no caso de falha 2 pois o tempo de geração da obrigatória fica irrelevante desde que > 0.*/
	}

	/* ----------------------------------------------- */
	/* Família LD - Caso C5                            */
	/* ----------------------------------------------- */

	public static void caseC5Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("1-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);
			
			new Trap21("2-21", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);
			
			new Trap42("3-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			//new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
			//Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			//new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			//Thread.sleep(50);

			//new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
			//Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Success5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail2(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			//new Trap42("2-42", params, addressParam, portParam, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail3(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 1202).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 2404).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail4(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 2404).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 1202).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail5(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap42("2-42", params, addressParam, portParam, millis + 601).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 1202).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void caseC5Fail6(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap42("2-42", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);

			new Trap21("1-21", params, addressParam, portParam, millis + 20).run();

			//new Trap17("3-17", params, addressParam, portParam, millis + 0).run();
			//Thread.sleep(50);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* ----------------------------------------------- */
	/* Família LD - Caso C8                            */
	/* ----------------------------------------------- */
	
	public static void caseC8Success1(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap67("1-67", params, addressParam, portParam, millis + 0).run();
			Thread.sleep(50);

			new Trap18("2-18", params, addressParam, portParam, millis + 10).run();
			Thread.sleep(50);
			
			new Trap22("3-22", params, addressParam, portParam, millis + 20).run();
			Thread.sleep(50);
			
			new Trap66("4-66", params, addressParam, portParam, millis + 30).run();
			Thread.sleep(50);
			
			new Trap68("5-68", params, addressParam, portParam, millis + 40).run();
			Thread.sleep(50);
//			
			new Trap38("6-38", params, addressParam, portParam, millis + 50).run();
			Thread.sleep(50);
			
			new Trap64("7-64", params, addressParam, portParam, millis + 60).run();
			Thread.sleep(50);
			
			new Trap58("8-21", params, addressParam, portParam, millis + 70).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
