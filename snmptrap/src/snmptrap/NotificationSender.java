package snmptrap;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import snmptrap.traps.gx.GxTrapSender;
import snmptrap.traps.gx.Trap201;
import snmptrap.traps.gx.Trap221;
import snmptrap.traps.gx.Trap244;
import snmptrap.traps.gx.Trap260;
import snmptrap.traps.ld.LdTrapSender;
import snmptrap.traps.ld.Trap17;
import snmptrap.traps.ld.Trap18;
import snmptrap.traps.ld.Trap21;
import snmptrap.traps.ld.Trap22;
import snmptrap.traps.ld.Trap38;
import snmptrap.traps.ld.Trap42;
import snmptrap.traps.ld.Trap58;
import snmptrap.traps.ld.Trap64;
import snmptrap.traps.ld.Trap66;
import snmptrap.traps.ld.Trap67;
import snmptrap.traps.ld.Trap68;
import snmptrap.traps.maple.MapleTrapSender;

public class NotificationSender
{
//	private static final String ADDRESS = "200.19.179.225";
	private static final String ADDRESS = "127.0.0.1";
	private static final int SNMP_PORT = 162;
	private static final int SYSLOG_PORT = 514;

	private static JSONObject params = null;

	public static void main(String args[])
	{
		readTrapParams();
		
//		MonitoringSender.send("./monitoring/sysUpTime#DOWN.mon");
//		MonitoringSender.send("./monitoring/sysUpTime#UP.mon");

//		MonitoringSender.send("./monitoring/memoryUsage#CRITICAL.mon");
//		MonitoringSender.send("./monitoring/memoryUsage#WARNING.mon");
//		MonitoringSender.send("./monitoring/memoryUsage#NORMAL.mon");

//		MonitoringSender.send("./Sending email to davydmelo@gmail.commonitoring/cpuLoad#CRITICAL.mon");
//		MonitoringSender.send("./monitoring/cpuLoad#WARNING.mon");
//		MonitoringSender.send("./monitoring/cpuLoad#NORMAL.mon");
			
		/* ----------------------------------------------- */
		/* Família GX                                      */
		/* ----------------------------------------------- */
		
//		GxTrapSender.caseC5_1_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C5.1_G2500.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C5.1_LD3032.log");
		
//		GxTrapSender.caseC5_2_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C5.2_G2500.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C5.2_LD3032.log");
		
//		GxTrapSender.caseC12_1_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C12.1.log");
		
//		GxTrapSender.caseC12_2_1_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C12.2.1.log");
		
//		GxTrapSender.caseC12_2_2_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/olt/C12.2.2.log");
		
		
		/* TESTES APRESENTAÇÃO SPRINT 8 */
		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 2L);
		
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/TESTE.log");
		
		//OK - 30 mar - timeout
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C7_OLT1_ONU2.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C8_OLT1_ONU2.log");
		
		//OK - 30 mar - timeout
//		GxTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C14.1_OLT1_ONU2.log");
//		GxTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C14.2_OLT1_ONU2.log");
		
		//OK - 30 mar - timeout
//		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
		//OK - 30 mar - timeout
//		onuDown_oltPortDown_GX_test_quantity_events();
//		onuUp_oltPortUp_GX_test_quantity_events();
		
//		onuDown_oltPortDown_GX_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_GX_test_ten_onu_up_plus_olt_port_up();
//		onuDown_oltPortDown_GX_test_all_ports();
		
//		test_generate_alarm();
//		test_not_generate_alarm();
		
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/test.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C8_OLT1_ONU2.log");
		
		
		/* TESTES APRESENTAÇÃO SPRINT 8 */

//		GxTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C2_OLT1_ONU2.log");
//		
//		GxTrapSender.caseC2Success2(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC2Success3(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC2Success4(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC2Success5(params, ADDRESS, SNMP_PORT);
//
//		GxTrapSender.caseC2Fail1(params, ADDRESS, SNMP_PORT);   //OK - ordely = false //TESTE
//		GxTrapSender.caseC2Fail2(params, ADDRESS, SNMP_PORT);   //OK - required = false
//		GxTrapSender.caseC2Fail3(params, ADDRESS, SNMP_PORT);   //OK - temporalCorrelation = false
//		GxTrapSender.caseC2Fail4(params, ADDRESS, SNMP_PORT);   // temporalCorrelation = false, ordely = false
//		GxTrapSender.caseC2Fail5(params, ADDRESS, SNMP_PORT);   // temporalCorrelation = false, ordely = false, sequence = false
//		GxTrapSender.caseC2Fail6(params, ADDRESS, SNMP_PORT);   // temporalCorrelation = false, sequence = false, required = false 
		
//		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);

		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 2L);
		params.put("onu_serial_number", "FIOG54009524");
		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		
		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 3L);
		params.put("onu_serial_number", "FIOG54009525");
		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		
//		params.put("gpon_olt_index", 3L);
//		params.put("onu_id", 4L);
//		params.put("onu_serial_number", "FIOG54009526");
//		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		
//		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		
//		params.put("gpon_olt_index", 3L);
//		params.put("onu_id", 5L);
//		params.put("onu_serial_number", "FIOG54009527");
////		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT); 
//		
//		params.put("gpon_olt_index", 3L);
//		params.put("onu_id", 6L);
//		params.put("onu_serial_number", "FIOG54009528");
////		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		
		
//		GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
//		delay(2000);
//		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C5_OLT1_ONU2.log");
		
//		GxTrapSender.caseC5Success2(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success3(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success4(params, ADDRESS, SNMP_PORT);
//		GxTrapSender.caseC5Success5(params, ADDRESS, SNMP_PORT);
//
//		GxTrapSender.caseC5Fail1(params, ADDRESS, SNMP_PORT);   //OK - ordely = false
//		GxTrapSender.caseC5Fail2(params, ADDRESS, SNMP_PORT);   //OK - required = false
//		GxTrapSender.caseC5Fail3(params, ADDRESS, SNMP_PORT);   //OK - temporalCorrelation = false
//		GxTrapSender.caseC5Fail4(params, ADDRESS, SNMP_PORT);   // temporalCorrelation = false, ordely = false
//		GxTrapSender.caseC5Fail5(params, ADDRESS, SNMP_PORT);   // temporalCorrelation = false, ordely = false, sequence = false
//		GxTrapSender.caseC5Fail6(params, ADDRESS, SNMP_PORT);
		
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C7_OLT1_ONU2.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C8_OLT1_ONU2.log");
		
//		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C8_OLT1_ONU2.log");
		
//		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		//GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C11.1_OLT.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C11.2_OLT.log");
		
//		GxTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C14.1_OLT1_ONU2.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C14.2_OLT1_ONU2.log");
//		GxTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);		

		/* ----------------------------------------------- */
		/* Família LD                                      */
		/* ----------------------------------------------- */
		
//		LdTrapSender.caseC5_1_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/olt/C5.1.log");
		
//		LdTrapSender.caseC5_2_OLT_Success(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/olt/C5.2.log");
		
		/* TESTES APRESENTAÇÃO SPRINT 8 */
//		params.put("gpon_olt_index", 3L);
//		params.put("onu_id", 2L);
		
		//OK - 24/03/20 - 17h30
//		LdTrapSender.caseC7Success1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		
		//OK - 24/03/20 - 17h30
//		LdTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C14.1_OLT1_ONU1.log");
//		LdTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C14.2_OLT1_ONU1.log");
		
		//OK - 24/03/20 - 17h30
//		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		//LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
		//OK - 24/03/20 - 18h00
//		onuDown_oltPortDown_LD_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_LD_test_ten_onu_up_plus_olt_port_up();
		/* TESTES APRESENTAÇÃO SPRINT 8 */

//		LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC2Success4(params, ADDRESS, SNMP_PORT);
//
//		LdTrapSender.caseC2Fail1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC2Fail2(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC2Fail3(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC2Fail4(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC2Fail5(params, ADDRESS, SNMP_PORT);
		
//		bug_bdeh_405();
//		bug_bdeh_410();
//		bug_bdeh_412();
		
//		LdTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C5_OLT5_ONU4.log");
		
//		LdTrapSender.caseC5Success2(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Success3(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Success4(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Success5(params, ADDRESS, SNMP_PORT);
//
//		LdTrapSender.caseC5Fail1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Fail2(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Fail3(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Fail4(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Fail5(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC5Fail6(params, ADDRESS, SNMP_PORT);
		
//		LdTrapSender.caseC7Success1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C2_OLT5_ONU4.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C5_OLT5_ONU4.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C8_OLT5_ONU4.log");
		
//		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
//		LdTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		LdTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C14.1_OLT1_ONU1.log");
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/C14.2_OLT1_ONU1.log");
		
//		onuDown_oltPortDown_GX_test_only_olt_port_down(); 
//		onuDown_oltPortDown_GX_test_ten_onu_down(); 
//		onuDown_oltPortDown_GX_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_GX_test_ten_onu_down_plus_olt_port_down_and_alarm_correction();
//		onuDown_oltPortDown_GX_test_all_ports();
//		
//		onuDown_oltPortDown_LD_test_only_olt_port_down(); 
//		onuDown_oltPortDown_LD_test_ten_onu_down(); 
//		onuDown_oltPortDown_LD_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_LD_test_ten_onu_down_plus_olt_port_down_and_alarm_correction();
		
//		
//		onuDown_oltPortDown_MAPLE_test_only_olt_port_down(); 
//		onuDown_oltPortDown_MAPLE_test_ten_onu_down(); 
//		onuDown_oltPortDown_MAPLE_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_MAPLE_test_ten_onu_down_plus_olt_port_down_and_alarm_correction();
		
		
//		MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		
//		for (int kuririn = 0; kuririn < 1; kuririn++) {
//////			MonitoringSender.send("./monitoring/cpuLoad#CRITICAL.mon");
////			onuDown_oltPortDown_LD_test_all_ports();
//			onuDown_oltPortDown_GX_test_all_ports();
//////			LdTrapSender.caseC7Success1(params, ADDRESS, SNMP_PORT);
//		}

		
		/* ----------------------------------------------- */
		/* Família MAPLE                                   */
		/* ----------------------------------------------- */
		
		/* TESTES SPRINT 8 */
		
		params.put("gpon_olt_index", 23L);
		params.put("onu_id", 22L);
		
//		MapleTrapSender.caseC7Success1(params, ADDRESS, SNMP_PORT);
//		MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		
//		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
		
//		MapleTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C14.1.log");
//		MapleTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C14.2.log");
		
//		onuDown_oltPortDown_MAPLE_test_ten_onu_down_plus_olt_port_down();
//		onuDown_oltPortDown_MAPLE_test_ten_onu_up_plus_olt_port_up();
		
		/* TESTES SPRINT 8 */

//		MapleTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);            //OK
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C2_OLT9_ONU2.log"); //OK

//		MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);            //OK
		//LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C5_OLT9_ONU2.log"); //OK

//		MapleTrapSender.caseC5Fail1(params, ADDRESS, SNMP_PORT);
//		MapleTrapSender.caseC5Fail2(params, ADDRESS, SNMP_PORT);
		
//		MapleTrapSender.caseC7Success1(params, ADDRESS, SNMP_PORT);
				
//		MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);            //OK
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C8_OLT9_ONU2.log");
		
//		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
//		LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/maple/C11.1_OLT.log");
		
//		MapleTrapSender.caseC14_1Success1(params, ADDRESS, SNMP_PORT);
//		MapleTrapSender.caseC14_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	// GX
	public static final void bug_bdeh_405() {
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, ADDRESS, SNMP_PORT, millis + 0).run();
			Thread.sleep(50);			

			new Trap244("2-244", params, ADDRESS, SNMP_PORT, millis + 10).run();
			Thread.sleep(50);

			new Trap260("3-260", params, ADDRESS, SNMP_PORT, millis + 30).run();
			Thread.sleep(50);

			new Trap201("4-201", params, ADDRESS, SNMP_PORT, millis + 20).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}
	
	public static final void bug_bdeh_410() {
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap67("1-67", params, ADDRESS, SNMP_PORT, millis + 0).run();
			Thread.sleep(50);

			new Trap18("2-18", params, ADDRESS, SNMP_PORT, millis + 10).run();
			Thread.sleep(50);
			
			new Trap22("3-22", params, ADDRESS, SNMP_PORT, millis + 20).run();
			Thread.sleep(50);
			
			new Trap66("4-66", params, ADDRESS, SNMP_PORT, millis + 30).run();
			Thread.sleep(50);

			new Trap38("6-38", params, ADDRESS, SNMP_PORT, millis + 40).run();
			Thread.sleep(50);
			
			new Trap64("7-64", params, ADDRESS, SNMP_PORT, millis + 50).run();
			Thread.sleep(50);
			
			new Trap58("8-58", params, ADDRESS, SNMP_PORT, millis + 60).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	} 
	
	
	public static final void bug_bdeh_412() {
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap17("1-17", params, ADDRESS, SNMP_PORT, millis + 0).run();
			Thread.sleep(50);
			
			LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/ld/bdeh_412.log");
			Thread.sleep(50);
			
			new Trap21("2-21", params, ADDRESS, SNMP_PORT, millis + 0).run();
			Thread.sleep(50);
			
			new Trap42("3-42", params, ADDRESS, SNMP_PORT, millis + 0).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	} 
	
	public static final void test_generate_alarm() {
		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 2L);
		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		delay(50);
		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 2L);
		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void test_not_generate_alarm() {
		params.put("onu_id", 2L);
		GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		delay(1500);
		params.put("gpon_olt_index", 3L);
		params.put("onu_id", 2L);
		GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_GX_test_only_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_GX_test_ten_onu_down() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		
	}
	
	public static final void onuDown_oltPortDown_GX_test_ten_onu_up() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		}
	}
	
	public static final void onuDown_oltPortDown_GX_test_quantity_events() {
		
//		for (long port = 1; port <= 64; port++) {
//			
//			params.put("gpon_olt_index", port);
//			
//			for (long onu = 1; onu <= 64; onu++) {
//				
//				params.put("onu_serial_number", String.valueOf(port) + "-" + String.valueOf(onu));
//				params.put("onu_id", (long) onu);
//				
//				GxTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//			}
//			
//			GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		}
		
//		params.put("gpon_olt_index", 3L);
//
//		for (int i = 1; i <= 32; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		
//		for (int i = 33; i <= 64; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		params.put("gpon_olt_index", 5L);
//		
//		for (int i = 1; i <= 10; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//		}
		
//		params.put("gpon_olt_index", 3L);
//
//		for (int i = 1; i <= 32; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//		
//		for (int i = 33; i <= 64; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		params.put("gpon_olt_index", 5L);
//		
//		for (int i = 1; i <= 10; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
//		}
		
		params.put("gpon_olt_index", 3L);

		for (int i = 1; i <= 32; i++) {
			params.put("onu_serial_number", "3-" + String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
		}
		
		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		
		for (int i = 33; i <= 64; i++) {
			params.put("onu_serial_number", "3-" + String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
		}
		
		params.put("gpon_olt_index", 5L);
		
		for (int i = 1; i <= 10; i++) {
			params.put("onu_serial_number", "5-" + String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
		}
		
				
	}
	
	public static final void onuUp_oltPortUp_GX_test_quantity_events() {
		
//		params.put("gpon_olt_index", 3L);
//
//		for (int i = 1; i <= 64; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);	
//		
//		params.put("gpon_olt_index", 5L);
//		
//		for (int i = 1; i <= 10; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
		
//		params.put("gpon_olt_index", 3L);
//
//		for (int i = 1; i <= 64; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);	
//		
//		params.put("gpon_olt_index", 5L);
//		
//		for (int i = 1; i <= 10; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
		
//		params.put("gpon_olt_index", 3L);
//
//		for (int i = 1; i <= 64; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
//		
//		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);	
//		
//		params.put("gpon_olt_index", 5L);
//		
//		for (int i = 1; i <= 10; i++) {
//			params.put("onu_serial_number", String.valueOf(i));
//			params.put("onu_id", (long) i);
//			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//		}
		
		params.put("gpon_olt_index", 3L);

		for (int i = 1; i <= 64; i++) {
			params.put("onu_serial_number", "5-" + String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		}
		
		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);	
		
		params.put("gpon_olt_index", 5L);
		
		for (int i = 1; i <= 11; i++) {
			params.put("onu_serial_number", "3-" + String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		}

	}
	
	public static final void onuDown_oltPortDown_GX_test_ten_onu_down_plus_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
		}
		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_GX_test_ten_onu_up_plus_olt_port_up() {
		params.put("gpon_olt_index", 6L);
		for (int i = 1; i < 65; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
		}
		GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_GX_test_ten_onu_down_plus_olt_port_down_and_alarm_correction() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		
		delay(30000);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_GX_test_all_ports() {
//		
//		System.out.println("Iteração down - início");
		for (long port = 1; port <= 1; port++) {
			params.put("gpon_olt_index", port);
			for (long onu = 1; onu <= 4; onu++) {
				//System.out.println("Iteração down " + port + "/" + onu);
				params.put("onu_serial_number", "FIOG5400952" + (3 + onu));
				params.put("onu_id", (long) onu);
				GxTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//				LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C2_OLT1_ONU2.log");
			}
			GxTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
//			LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C11.1_OLT.log");
			
		}
//		System.out.println("Iteração down - fim");
//		
//		delay(30000);
//		
////		System.out.println("Iteração up - início");
//		for (long port = 1; port <= 1; port++) {
//			params.put("gpon_olt_index", port);
//			for (long onu = 1; onu <= 4; onu++) {
//				//System.out.println("Iteração down " + port + "/" + onu);
//				params.put("onu_serial_number", "FIOG5400952" + (3 + onu));
//				params.put("onu_id", (long) onu);
//				GxTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
////				LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C8_OLT1_ONU2.log");
//			}
//			GxTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
////			LogSender.send(ADDRESS, SYSLOG_PORT, "./logs/gx/C11.2_OLT.log");
//		}
//		System.out.println("Iteração up - fim");
	}
	
	public static final void onuDown_oltPortDown_LD_test_only_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_LD_test_ten_onu_down_c2() {
		params.put("gpon_olt_index", 3L);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
			delay(50);
		}
	}
	
	public static final void onuDown_oltPortDown_LD_test_ten_onu_down() {
		params.put("gpon_olt_index", 3L);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
	}
	
	public static final void onuDown_oltPortDown_LD_test_ten_onu_down_plus_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 65; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC2Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
//		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_LD_test_ten_onu_up_plus_olt_port_up()
	{
		params.put("gpon_olt_index", 3L);
		
		for (int i = 1; i < 10; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
//		LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_LD_test_ten_onu_down_plus_olt_port_down_and_alarm_correction() {
		params.put("gpon_olt_index", 3L);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		
		delay(10000);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_LD_test_all_ports() {
		
		System.out.println("Iteração down - início");
		for (long port = 1; port <= 10; port++) {
			params.put("gpon_olt_index", port);
			for (long onu = 1; onu <= 10; onu++) {
				//System.out.println("Iteração down " + port + "/" + onu);
				params.put("onu_serial_number", String.valueOf(port) + String.valueOf(onu));
				params.put("onu_id", (long) onu);
				LdTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			}
//			LdTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		}
		System.out.println("Iteração down - fim");
		
//		delay(30000);
//		
//		System.out.println("Iteração up - início");
//		for (long port = 1; port <= 10; port++) {
//			params.put("gpon_olt_index", port);
//			for (long onu = 1; onu <= 10; onu++) {
//				//System.out.println("Iteração down " + port + "/" + onu);
//				params.put("onu_serial_number", String.valueOf(port) + String.valueOf(onu));
//				params.put("onu_id", (long) onu);
//				LdTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//			}
////			LdTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
//		}
//		System.out.println("Iteração up - fim");
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_only_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_ten_onu_down() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_ten_onu_down_plus_olt_port_down() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_ten_onu_up_plus_olt_port_up() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_ten_onu_down_plus_olt_port_down_and_alarm_correction() {
		params.put("gpon_olt_index", 3L);
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		
		delay(30000);
		
		for (int i = 1; i < 11; i++) {
			params.put("onu_serial_number", String.valueOf(i));
			params.put("onu_id", (long) i);
			MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
			delay(50);	
		}
		MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
	}
	
	public static final void onuDown_oltPortDown_MAPLE_test_all_ports() {
		
		System.out.println("Iteração down - início");
		for (long port = 1; port <= 64; port++) {
			params.put("gpon_olt_index", port);
			for (long onu = 1; onu <= 64; onu++) {
				//System.out.println("Iteração down " + port + "/" + onu);
				params.put("onu_serial_number", String.valueOf(port) + String.valueOf(onu));
				params.put("onu_id", (long) onu);
				MapleTrapSender.caseC5Success1(params, ADDRESS, SNMP_PORT);
//				delay(200);
			}
//			MapleTrapSender.caseC11_1Success1(params, ADDRESS, SNMP_PORT);
		}
		System.out.println("Iteração down - fim");
		
//		delay(10000);
//		
//		System.out.println("Iteração up - início");
//		for (long port = 1; port <= 1; port++) {
//			params.put("gpon_olt_index", port);
//			for (long onu = 1; onu <= 8; onu++) {
//				//System.out.println("Iteração down " + port + "/" + onu);
//				params.put("onu_serial_number", String.valueOf(port) + String.valueOf(onu));
//				params.put("onu_id", (long) onu);
//				MapleTrapSender.caseC8Success1(params, ADDRESS, SNMP_PORT);
//			}
//			MapleTrapSender.caseC11_2Success1(params, ADDRESS, SNMP_PORT);
//		}
//		System.out.println("Iteração up - fim");
	}
	
	private static final void delay(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static final void readTrapParams() {
		
		JSONParser jsonParser = new JSONParser();
		
		try (FileReader reader = new FileReader("./dist/sample.json"))
		{
			params = (JSONObject) jsonParser.parse(reader);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
