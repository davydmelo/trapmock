package snmptrap.cases;

import java.util.Random;

import org.json.simple.JSONObject;

import snmptrap.traps.gx.Trap201;
import snmptrap.traps.gx.Trap221;
import snmptrap.traps.gx.Trap223;
import snmptrap.traps.gx.Trap244;
import snmptrap.traps.gx.Trap260;

public class CaseC5
{
	public static void execute(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;

		try
		{
			new Trap221("1-221", params, addressParam, portParam, millis + new Random().nextInt(10)).run();
			Thread.sleep(50);

			new Trap244("2-244", params, addressParam, portParam, millis + new Random().nextInt(10)).run();
			Thread.sleep(50);

			new Trap223("3-223", params, addressParam, portParam, millis + new Random().nextInt(10)).run();
			Thread.sleep(50);

			new Trap260("4-260", params, addressParam, portParam, millis + new Random().nextInt(10)).run();
			Thread.sleep(50);

			new Trap201("5-201", params, addressParam, portParam, millis + new Random().nextInt(10)).run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void executeAsync(JSONObject params, String addressParam, int portParam)
	{
		long millis = System.currentTimeMillis()/1000;
		
		new Thread(new Trap221("1-221", params, addressParam, portParam, millis)).start();
		new Thread(new Trap244("2-244", params, addressParam, portParam, millis)).start();
		new Thread(new Trap223("3-223", params, addressParam, portParam, millis)).start();
		new Thread(new Trap260("4-260", params, addressParam, portParam, millis)).start();
		new Thread(new Trap201("5-201", params, addressParam, portParam, millis)).start();
	}
}
