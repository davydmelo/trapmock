package snmptrap.cases;

import org.json.simple.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

public class CaseC5Up
{
	public static void execute(JSONObject params, String addressParam, int portParam)
	{
		//sendFirstTrap(params, addressParam, portParam);
		//sendSecondTrap(params, addressParam, portParam); 
		sendThirdTrap(params, addressParam, portParam); 
	}
	
	private static void sendFirstTrap(JSONObject params, String addressParam, int portParam)
	{
		try
		{
			PDU pdu = new PDU();
			pdu.setType(PDU.TRAP);
			pdu.setRequestID(new Integer32(123));

			pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new TimeTicks(System.currentTimeMillis()/1000)));
			pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID("1.3.6.1.4.1.10428.9.1.0.224")));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.2.1.1.1.0"), new Integer32(((Long)params.get("gpon_olt_index")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.3.1.1.1.0"), new Integer32(((Long)params.get("onu_id")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.3.1.1.4.0"), new OctetString(params.get("onu_serial_number").toString())));

			TransportMapping<?> transport = new DefaultUdpTransportMapping();

			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString("public"));
			comtarget.setVersion(SnmpConstants.version2c);
			comtarget.setAddress(new UdpAddress(addressParam + "/" + portParam));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			Snmp snmp = new Snmp(transport);
			snmp.send(pdu, comtarget);
			System.out.println("Sent Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			snmp.close();
		}
		catch (Exception e)
		{
			System.err.println("Error in Sending Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}

	private static void sendSecondTrap(JSONObject params, String addressParam, int portParam)
	{
		try
		{
			PDU pdu = new PDU();
			pdu.setType(PDU.TRAP);
			pdu.setRequestID(new Integer32(123));

			pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new TimeTicks(System.currentTimeMillis()/1000)));
			pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID("1.3.6.1.4.1.10428.9.1.0.222")));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.2.1.1.1.0"), new Integer32(((Long)params.get("gpon_olt_index")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.3.1.1.1.0"), new Integer32(((Long)params.get("onu_id")).intValue())));

			TransportMapping<?> transport = new DefaultUdpTransportMapping();

			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString("public"));
			comtarget.setVersion(SnmpConstants.version2c);
			comtarget.setAddress(new UdpAddress(addressParam + "/" + portParam));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			Snmp snmp = new Snmp(transport);
			snmp.send(pdu, comtarget);
			System.out.println("Sent Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			snmp.close();
		}
		catch (Exception e)
		{
			System.err.println("Error in Sending Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}

	private static void sendThirdTrap(JSONObject params, String addressParam, int portParam)
	{
		try
		{
			PDU pdu = new PDU();
			pdu.setType(PDU.TRAP);
			pdu.setRequestID(new Integer32(123));

			pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new TimeTicks(System.currentTimeMillis()/1000)));
			pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID("1.3.6.1.4.1.10428.9.1.0.200")));
			pdu.add(new VariableBinding(new OID("1.3.6.1.2.1.1.1.0"), new OctetString(params.get("olt_description").toString())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.2.1.1.1.0"), new Integer32(((Long)params.get("gpon_olt_index")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.3.1.1.1.0"), new Integer32(((Long)params.get("onu_id")).intValue())));
			pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.10428.9.1.101.23.3.1.1.15.0"), new Integer32(2)));		

			TransportMapping<?> transport = new DefaultUdpTransportMapping();

			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString("public"));
			comtarget.setVersion(SnmpConstants.version2c);
			comtarget.setAddress(new UdpAddress(addressParam + "/" + portParam));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			Snmp snmp = new Snmp(transport);
			snmp.send(pdu, comtarget);
			System.out.println("Sent Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			snmp.close();
		}
		catch (Exception e)
		{
			System.err.println("Error in Sending Trap to (IP:Port)=> " + addressParam + ":" + portParam);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}
}
