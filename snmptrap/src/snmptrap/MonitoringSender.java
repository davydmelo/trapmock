package snmptrap;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

public class MonitoringSender {
	public static void send(String filename) {
		try {

			HttpsTrustManager.allowAllSSL();
			
			URL url = new URL("https://localhost:5043");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			String auth = "furukawa" + ":" + "a64456611b1dc3c5305739477a6a857d";
			byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
			String authHeaderValue = "Basic " + new String(encodedAuth);

			con.setRequestProperty("Authorization", authHeaderValue);
			con.setRequestMethod("GET");
			con.setDoOutput(true);

			DataOutputStream out = new DataOutputStream(con.getOutputStream());

			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = null;
			
			do {
				
				line = reader.readLine();
				if (line != null)
					out.writeBytes(line);
				
			} while (line != null); 
			
			out.flush();
			out.close();

			System.out.println(con.getResponseCode());

			con.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
