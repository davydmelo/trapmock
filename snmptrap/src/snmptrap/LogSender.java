package snmptrap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LogSender {
	public static void send(String address, int port, String filename) {
		BufferedReader reader;
		try{
			reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			
			while (line != null) {
				
				if (line != null) {
					
					System.out.println(line);

					List<String> commands = new ArrayList<String>();
					commands.add("bash");
					commands.add("-c");
					commands.add("logger -n " + address + " -P " + 514 + " --rfc3164 " + "\"" + line + "\"");
					
					ProcessBuilder pb = new ProcessBuilder(commands);
					
					Process process = pb.start();
					
					StringBuilder out = new StringBuilder();
			        BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			        String linea = null, previous = null;
			        while ((linea = br.readLine()) != null)
			            if (!linea.equals(previous)) {
			                previous = linea;
			                out.append(linea).append('\n');
			                System.out.println(linea);
			            }
				}
				
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
