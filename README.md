O Eclipse deverá executar o método main() da classe TrapSender. Na Run Configuration -> Aba Arguments devemos colocar os seguintes parametros: "-i dist/sample.json -d 1 -r 1 -p 162 -c 2 -a 200.19.179.225 -t 0".
Para selecionar o que vai ser enviado, basta descomentar as chamadas de métodos dentro do método execute() da classe CaseC2. ;D

<!--**java -jar trapmock.jar -i sample.json -d 2000 -r 3 -p 1062 -c 5 -a fkcp-logstash**    -->

<!--**-a**,--address <arg>   Destination IP of the trap.    -->
<!--**-c**,--case <arg>      The case to test.    -->
<!--**-d**,--delay <arg>     Delay between rounds (ms).    -->
<!--**-i**,--input <arg>     JSON file path.    -->
<!--**-p**,--port <arg>      Destination port of the trap.    -->
<!--**-r**,--rounds <arg>    Number of rounds.    -->